import commons from './commons.json'
import landing from './landing.json'
import modals from './modals.json'
import auth from './auth.json'

export default { commons, landing, modals, auth }
