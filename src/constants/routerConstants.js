export const RootPath = process.env.NODE_ENV === 'production' ? '' : ''

export const HomePath = `${RootPath}/`
export const LoginInPath = `${RootPath}/login`
export const SignInPath = `${RootPath}/signup`
export const LogOutPath = `${RootPath}/logout`
export const TravelPath = `${RootPath}/travel`
export const ClaimPath = `${RootPath}/claim`

export const tripvixiaURL = 'https://login.destinare.io/'
export const dappURL = 'https://dapp.destinare.io/'
