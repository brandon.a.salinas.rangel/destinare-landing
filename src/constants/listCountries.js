export const countries = [
    { key: 18, value: 'Argentina-Buenos Aires' },
    { key: 11, value: 'Australia-Sydney' },
    { key: 14, value: 'Australia-Brisbane' },
    { key: 12, value: 'Australia-Melbourne' },
    { key: 13, value: 'Australia-Gold Coast' },
    { key: 7, value: 'Austria-Vienna' },
    { key: 23, value: 'Brazil-Rio de Janeiro' },
    { key: 26, value: 'Canada-Toronto' },
    { key: 24, value: 'Colombia-Medellín' },
    { key: 25, value: 'Colombia-Cartagena' },
    { key: 2, value: 'Dominican Republic-Punta Cana' },
    { key: 166, value: 'Fiji-Fiji' },
    { key: 16, value: 'France-Paris' },
    { key: 19, value: 'Greece-Athens' },
    { key: 61, value: 'Hungary-Budapest' },
    { key: 66, value: 'India-Goa' },
    { key: 49, value: 'Indonesia-Bali' },
    { key: 46, value: 'Israel-Jerusalem' },
    { key: 28, value: 'Italy-Venice' },
    { key: 29, value: 'Italy-Rome' },
    { key: 9, value: 'Mexico-Mazatlán' },
    { key: 48, value: 'Mexico-Puerto Vallarta' },
    { key: 10, value: 'Mexico-Cancún' },
    { key: 6, value: 'Philippines-Boracay' },
    { key: 62, value: 'Russia-Moscow' },
    { key: 17, value: 'South Africa-Cape Town' },
    { key: 63, value: 'Spain-Tenerife' },
    { key: 27, value: 'Spain-Barcelona' },
    { key: 5, value: 'Sweden-Stockholm' },
    { key: 3, value: 'Thailand-Phuke' },
    { key: 50, value: 'Thailand-Koh Samui' },
    { key: 1, value: 'Thailand-Bangkok' },
    { key: 21, value: 'Turkey-Istanbul' },
    { key: 22, value: 'Turkey-Bodrum' },
    { key: 34, value: 'U.S.A.-Orlando' },
    { key: 37, value: 'U.S.A.-Chicago' },
    { key: 42, value: 'U.S.A.-Atlantic City' },
    { key: 36, value: 'U.S.A.-Hawaii' },
    { key: 45, value: 'U.S.A.-San Antonio' },
    { key: 44, value: 'U.S.A.-Gatlinburg' },
    { key: 39, value: 'U.S.A.-New Orleans' },
    { key: 33, value: 'U.S.A.-Daytona Beach' },
    { key: 40, value: 'U.S.A.-Branson' },
    { key: 43, value: 'U.S.A.-Myrtle Beach' },
    { key: 35, value: 'U.S.A.-Atlanta' },
    { key: 41, value: 'U.S.A.-Las Vegas' },
    { key: 31, value: 'U.S.A.-Phoenix' },
    { key: 32, value: 'U.S.A.-San Diego' },
    { key: 20, value: 'United Arab Emirates-Dubai' },
    { key: 167, value: 'Vanuatu-Vanuatu' },
]

// 4- none
// 15- Christchurch
// 30- U.S.A., FL
// 38- none
// 47 - none
// 51 - 60 - none
// 64 - none
// 65- Goa
// 67 - 165 - none
